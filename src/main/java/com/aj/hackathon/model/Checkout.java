package com.aj.hackathon.model;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
@Entity
@Table(name = "checkout")
public class Checkout {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long checkid;
	@ManyToOne
	private User user;
	@ManyToOne 
	private HospitalDetails hospitaldetails;
	private Long vaccinenos;
	private Long nbednos;
	private Long ibednos;
	private String time;
	private String category;
	//@Temporal(TemporalType.DATE)
	//@DateTimeFormat(pattern="dd/mm/yyyy")
	private String date; 
	private Date ordercreation;
	public Checkout()
	{
		
	}
	public Long getCheckid() {
		return checkid;
	}
	public void setCheckid(Long checkid) {
		this.checkid = checkid;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public HospitalDetails getHospitaldetails() {
		return hospitaldetails;
	}
	public void setHospitaldetails(HospitalDetails hospitaldetails) {
		this.hospitaldetails = hospitaldetails;
	}
	public Long getVaccinenos() {
		return vaccinenos;
	}
	public void setVaccinenos(Long vaccinenos) {
		this.vaccinenos = vaccinenos;
	}
	public Long getNbednos() {
		return nbednos;
	}
	public void setNbednos(Long nbednos) {
		this.nbednos = nbednos;
	}
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	//	public Date getDate() {
//		return date;
//	}
//	public void setDate(Date date) { 
//		this.date = date;
//	}
	public Date getOrdercreation() {
		return ordercreation;
	}
	public void setOrdercreation(Date ordercreation) {
		this.ordercreation = ordercreation;
	}
	
	public Long getIbednos() {
		return ibednos;
	}
	public void setIbednos(Long ibednos) {
		this.ibednos = ibednos;
	}
	public Checkout(Long checkid, User user, HospitalDetails hospitaldetails, Long vaccinenos, Long nbednos,
			Long ibednos, String time, String category, String date, Date ordercreation) {
		super();
		this.checkid = checkid;
		this.user = user;
		this.hospitaldetails = hospitaldetails;
		this.vaccinenos = vaccinenos;
		this.nbednos = nbednos;
		this.ibednos = ibednos;
		this.time = time;
		this.category = category;
		this.date = date;
		this.ordercreation = ordercreation;
	}
	
	
	

}
