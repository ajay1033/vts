package com.aj.hackathon.model;

public class Category {
	private String state;
	private String city;
	private String category;
	public Category()
	{
		
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Category(String state, String city, String category) {
		super();
		this.state = state;
		this.city = city;
		this.category = category;
	}

}
