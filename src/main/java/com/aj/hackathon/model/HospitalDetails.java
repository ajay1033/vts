package com.aj.hackathon.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hospitaldetails")
public class HospitalDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long hid;
	private String hname;
	private String hcity;
	private String hstate;
	private Long ihbeds_count;
	private Long nhbeds_count;
	private Long hvaccine_count;
	public HospitalDetails()
	{
		
	}
	public Long getHid() {
		return hid;
	}
	public void setHid(Long hid) {
		this.hid = hid;
	}
	public String getHname() {
		return hname;
	}
	public void setHname(String hname) {
		this.hname = hname;
	}
	public String getHcity() {
		return hcity;
	}
	public void setHcity(String hcity) {
		this.hcity = hcity;
	}
	public String getHstate() {
		return hstate;
	}
	public void setHstate(String hstate) {
		this.hstate = hstate;
	}
	public Long getIhbeds_count() {
		return ihbeds_count;
	}
	public void setIhbeds_count(Long ihbeds_count) {
		this.ihbeds_count = ihbeds_count;
	}
	public Long getNhbeds_count() {
		return nhbeds_count;
	}
	public void setNhbeds_count(Long nhbeds_count) {
		this.nhbeds_count = nhbeds_count;
	}
	public Long getHvaccine_count() {
		return hvaccine_count;
	}
	public void setHvaccine_count(Long hvaccine_count) {
		this.hvaccine_count = hvaccine_count;
	}
	public HospitalDetails(Long hid, String hname, String hcity, String hstate, Long ihbeds_count, Long nhbeds_count,
			Long hvaccine_count) {
		super();
		this.hid = hid;
		this.hname = hname;
		this.hcity = hcity;
		this.hstate = hstate;
		this.ihbeds_count = ihbeds_count;
		this.nhbeds_count = nhbeds_count;
		this.hvaccine_count = hvaccine_count;
	}
	
	public HospitalDetails(Long ihbeds_count, Long nhbeds_count, Long hvaccine_count) {
		super();
		this.ihbeds_count = ihbeds_count;
		this.nhbeds_count = nhbeds_count;
		this.hvaccine_count = hvaccine_count;
	}
	@Override
	public String toString() {
		return "HospitalDetails [hid=" + hid + ", hname=" + hname + ", hcity=" + hcity + ", hstate=" + hstate
				+ ", ihbeds_count=" + ihbeds_count + ", nhbeds_count=" + nhbeds_count + ", hvaccine_count="
				+ hvaccine_count + "]";
	}
	

}
