package com.aj.hackathon.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.aj.hackathon.model.Admin;
import com.aj.hackathon.model.HospitalDetails;
import com.aj.hackathon.model.VaccineAndBed;
import com.aj.hackathon.repository.AdminRepository;
import com.aj.hackathon.service.AdminService;
import com.aj.hackathon.twilio.TwilioController;

@Controller
public class AdminController {
	@Autowired
	AdminRepository arepo;
	@Autowired
	AdminService aservice;
@Autowired
TwilioController t;
	@GetMapping("/admin")
	public String login() {
		return "admin";

	}

	@GetMapping("/viewhospitaldetails")
	public String viewhospitaldetails(Model model) {
		List<HospitalDetails> list = arepo.findAll();
		System.out.println(list);
		model.addAttribute("list", list);
		return "hospitaldetails";
	}

	@GetMapping("/deletedetails")
	public String deletehospitaldetails(@RequestParam Long hid, Model model) {
		aservice.deletehospitaldetails(hid);
		List<HospitalDetails> list = arepo.findAll();

		model.addAttribute("list", list);
		return "hospitaldetails";
	}

	@PostMapping("/displayhospitaldetails")
	public String displayhospitaldetails(Admin admin, Model model) {
		System.out.println(admin.getName());
		System.out.println(admin.getPassword());
		if (admin.getName().equalsIgnoreCase("admin") && admin.getPassword().equalsIgnoreCase("admin")) {
			List<HospitalDetails> list = arepo.findAll();
			System.out.println(list);
			model.addAttribute("list", list);
			return "hospitaldetails";
		}
		model.addAttribute("error", "Invalid Credetials");
		return "admin";
	}

	@GetMapping("/addhospitaldetails")
	public String addhospitaldetailspage() {
		return "addhospitaldetails";
	}

	@PostMapping("/addhospitaldetails")
	public String hospitaldetailsadded(HospitalDetails h) {
		aservice.addetails(h);
		return "addhospitaldetails";
	}
	@PostMapping("/updatehospitalpost")
	public String hospitaldetailsupdatepost(@RequestParam Long hid,@RequestParam Long n,@RequestParam Long i,@RequestParam Long v,Model model) {
		
		List<HospitalDetails> list = arepo.findAll();
System.out.println(n);
		model.addAttribute("list", list);
		aservice.updatedetails(hid,n,i,v);
	
		return "hospitaldetails";
	}
	@GetMapping("/updatehospitaldetails")
	public String hospitaldetailsupdateget(@RequestParam Long hid,Model model) {
		
		List<HospitalDetails> list = arepo.findAll().stream().filter(n->n.getHid()==hid).collect(Collectors.toList());
System.out.println(list.toString());
		model.addAttribute("list", list.get(0));
		
		return "updatehospital";
	}
}
