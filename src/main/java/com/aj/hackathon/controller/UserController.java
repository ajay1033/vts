package com.aj.hackathon.controller;


import java.util.List;

import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.aj.hackathon.model.Admin;
import com.aj.hackathon.model.Category;
import com.aj.hackathon.model.Checkout;
import com.aj.hackathon.model.HospitalDetails;
import com.aj.hackathon.model.User;
import com.aj.hackathon.repository.AdminRepository;
import com.aj.hackathon.repository.HospitalRepository;
import com.aj.hackathon.repository.UserRepository;
import com.aj.hackathon.service.UserService;
import com.aj.hackathon.twilio.TwilioController;
import com.google.gson.Gson;

@Controller
@SessionAttributes({"em","c","hid"})
public class UserController {
    @Autowired
    UserService uservice;
    @Autowired
    UserRepository urepo;
    @Autowired
    AdminRepository arepo;
    @Autowired
    HospitalRepository hrepo;
    @Autowired
    TwilioController t;

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/register")
    public String register() {
        return "register";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @PostMapping("/postregister")
    public String postregister(User user,ModelMap model) {
    	 List<User> list = urepo.findAll();
    	 if(list.stream().filter(n->user.getEmail().equals(n.getEmail())).findAny().isPresent())
    	 {
    		 model.addAttribute("error", "Account already exists");
    		 return "register";
    	 }
        uservice.adduserdetails(user);
        return "login";
    }

    @PostMapping("/postlogin")
    public String postlogin(User user, Model model,ModelMap modelmap) {
      
     modelmap.put("em", user.getEmail());
        List<User> list = urepo.findAll();
        if (list.stream().filter(n -> n.getEmail().equals(user.getEmail())).findAny().isPresent()
                && list.stream().filter(n -> user.getPassword().equals(n.getPassword())).findAny().isPresent()) {
        	 List<String> list2 = arepo.findAll().stream().map(n -> n.getHstate()).distinct().collect(Collectors.toList());
             model.addAttribute("listCategory", list2);
             List<String> list1 = arepo.findAll().stream().map(n -> n.getHcity()).distinct().collect(Collectors.toList());
             model.addAttribute("listCategory1", list1);
            return "bookslot";
        }
        model.addAttribute("error", "Invalid Credentials");
        return "login";
    }

    @GetMapping("/bookslot")
    public String bookslot(Model model) {
    	//List<HospitalDetails> list=arepo.findAll();
    	 List<String> list = arepo.findAll().stream().map(n -> n.getHstate()).distinct().collect(Collectors.toList());
         model.addAttribute("listCategory", list);
        // String a=list.get(0);
       List<String> list1 = arepo.findAll().stream().map(n -> n.getHcity()).distinct().collect(Collectors.toList());
         model.addAttribute("listCategory1", list1);
        return "bookslot";
    }

    @PostMapping("/hospitaldetailsuser")
    public String displayhospitaldetailsforuser(Category category, ModelMap model) {
        List<HospitalDetails> list = arepo.findAll();
        model.put("c", category.getCategory());
        if (category.getCategory().equalsIgnoreCase("MildCase")) {
            model.addAttribute("list", list.stream().filter(n -> n.getHcity().equalsIgnoreCase(category.getCity())&& n.getHstate().equals(category.getState()) && n.getHvaccine_count() >= 1  ).collect(Collectors.toList()));
            return "hospitaldetailsuser";
        }
        else if (category.getCategory().equalsIgnoreCase("ModerateCase")) { 
        	model.addAttribute("list", list.stream().filter(n -> n.getHcity().equalsIgnoreCase(category.getCity())&& n.getHstate().equals(category.getState()) && n.getNhbeds_count() >= 1 && n.getHvaccine_count() >= 1   ).collect(Collectors.toList()));
            return "hospitaldetailsuser";
        }
        else {
        	model.addAttribute("list", list.stream().filter(n -> n.getHcity().equalsIgnoreCase(category.getCity()) && n.getHstate().equals(category.getState()) && n.getIhbeds_count() >= 1 && n.getHvaccine_count() >= 1  ).collect(Collectors.toList()));
            return "hospitaldetailsuser";
        }

    } 

    @GetMapping("/bookhospital")
    private String bookhospital(ModelMap model,@RequestParam Long hid,Checkout check) {
    	model.put("hid", hid);
    	 HospitalDetails hospitalmodel = arepo.getOne(hid);
    	String email=(String) model.get("em");
    	User usermodel=urepo.findByEmail(email);
    	model.addAttribute("u",usermodel);
    	String category=(String) model.get("c");
    	if(category.equalsIgnoreCase("MildCase"))
		{
				
			model.addAttribute("i","0");
			model.addAttribute("n","0");
			model.addAttribute("v","1");
		}
		else if(category.equalsIgnoreCase("ModerateCase"))
		{
			model.addAttribute("i","0");
			model.addAttribute("n","1");
			model.addAttribute("v","1");
		}
		else
		{
			model.addAttribute("i","1");
			model.addAttribute("n","0");
			model.addAttribute("v","1");
		}
    	model.addAttribute("h",hospitalmodel);
        return "bookhospital";

    }
    @PostMapping("/bookhospitalpost")
    private String bookhospitalpost(ModelMap model,Checkout check) {
    	 HospitalDetails hospitalmodel = arepo.getOne((Long) model.get("hid"));
    	String email=(String) model.get("em");
    	String category=(String) model.get("c");
    	User usermodel=urepo.findByEmail(email);
    	uservice.addcheckout(hospitalmodel,usermodel,check,category);
    	t.sendSMS(usermodel,check,hospitalmodel,category);
        return "login";

    }

  
    }


