package com.aj.hackathon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aj.hackathon.model.HospitalDetails;
@Repository
public interface AdminRepository extends JpaRepository<HospitalDetails, Long> {
public List<HospitalDetails> findByHstate(String hstate);
}
