package com.aj.hackathon.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aj.hackathon.model.Checkout;

public interface CheckoutRepository extends JpaRepository<Checkout, Long> {

}
