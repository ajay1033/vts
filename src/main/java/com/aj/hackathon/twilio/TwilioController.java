package com.aj.hackathon.twilio;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.aj.hackathon.model.Checkout;
import com.aj.hackathon.model.HospitalDetails;
import com.aj.hackathon.model.User;
import com.aj.hackathon.repository.HospitalRepository;
import com.aj.hackathon.repository.UserRepository;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
 
@Controller
public class TwilioController {
	// Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "AC309406630a4c9012f0933386faff8292";
    public static final String AUTH_TOKEN = "1d9a9da392ade799bed3e101c737f28a";
    public static final String TWILIO_NUMBER = "+18643340360";
    
    @Autowired
    UserRepository urepo;
    @Autowired
    HospitalRepository hrepo;

public void sendSMS(User user,Checkout check,HospitalDetails hospdetails,String category) {
    try {
        TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);

        // Build a filter for the MessageList
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("Body", "Name: "+user.getName()+"\nAge: "+user.getAge()+"\nHospital name & Location: "+hospdetails.getHname()+" & "+hospdetails.getHcity()+", "+hospdetails.getHstate()+
        		"\nCategory: "+category+"\nDate: "+check.getDate()+"\nTime :"+check.getTime()+"\nVaccine: "+check.getVaccinenos()+"\nNormal Beds: "+check.getNbednos()+"\nICU Beds: "+check.getIbednos()));
        params.add(new BasicNameValuePair("To", "+91"+user.getPhonenumber())); //Add real number here
        System.out.println("+91"+user.getPhonenumber());
        params.add(new BasicNameValuePair("From", TWILIO_NUMBER));

        MessageFactory messageFactory = client.getAccount().getMessageFactory();
        Message message = messageFactory.create(params);
        System.out.println(message.getSid());
    } 
    catch (TwilioRestException e) {
        System.out.println(e.getErrorMessage());
    }

}
public void notify(Long hid, Long n2, Long i2, Long v) {
    try {
        TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
HospitalDetails hospdetails=hrepo.getOne(hid);
        // Build a filter for the MessageList
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("Body", "Stocks are back in "+hospdetails.getHname()+" with vaccine:"+v+", Normal Bed:"+n2+", ICU Bed:"+i2));
       List<User> u= urepo.findAll().stream().filter(n->hospdetails.getHstate().equalsIgnoreCase(n.getState())).collect(Collectors.toList());
      
       for(int i=0;i<u.size();i++)
      {
       params.add(new BasicNameValuePair("To", "+91"+u.get(i).getPhonenumber())); //Add real number here
        System.out.println("+91"+u.get(i).getPhonenumber());
        params.add(new BasicNameValuePair("From", TWILIO_NUMBER));

        MessageFactory messageFactory = client.getAccount().getMessageFactory();
        Message message = messageFactory.create(params);
        System.out.println(message.getSid());
      }
    } 
    catch (TwilioRestException e) {
        System.out.println(e.getErrorMessage());
    }

}
}
