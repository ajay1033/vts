/*
This is a fabricated demo to show how environment
variables are handled in Bitbucket Pipelines.

In practice you might reference environment variables
as part of your deployment process, for example your
AWS or Azure keys might be set as environment variables
and used in code as part of the deployment.
*/

console.log('Set in the YAML file:', process.env.BITBUCKET_BRANCH)


if (process.env.BITBUCKET_BRANCH=== 'master') {
	console.log('The environment variable is set as master.')
}
else
{
console.log('someother branch')
}

